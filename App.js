import React, { Component } from 'react'
import { Text, View, Image, ScrollView, FlatList, TouchableOpacity, StyleSheet, Alert } from 'react-native'
import ImagePicker from 'react-native-image-crop-picker';
import Spinner from 'react-native-loading-spinner-overlay';
import RNFetchBlob from 'rn-fetch-blob'

export class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      loading: false,
      images: [],
      count: 0
    }
  }

  _toggleLoader = () => {
    this.setState({
      loading: !this.state.loading
    })
  }

  _uploadImages = (filePath) => {
    let rand = Math.floor(1000 + Math.random() * (1000 - 1))
    let unique_id = "BNL0" + rand
    let unique_identifier = "BKLSS" + rand

    let url = "https://asset-geotag.herokuapp.com/tags/createTagByAgent?tagType=Tag&lat=17.387140&lng=78.491684&unique_id=" + unique_id + "&unique_identifier=" + unique_identifier + "&tag_subcategory_id=Hoardings&project_agency_assign_id=5b069050-5445-4bc8-9c1a-e96f60a91979&created_by=249cbdfc-17c4-4c01-b750-f4dcb3b3d4f9"
    //console.log('uniqud id ', unique_id, "\t", unique_identifier, "\n", url)
    RNFetchBlob.fetch('POST', url, {
      'Content-Type': 'multipart/form-data',
      "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1OTYzMDY2ODgsImV4cCI6MTU5NzE3MDY4OH0.MNI0385ofYEjpA05fW2CUQ4bkK_3eAH9gpnojRpKxsA"
    }, [
      {
        name: 'tagImage', filename: "png",
        type: "png",
        data: RNFetchBlob.wrap(filePath)
      },
    ]).then((resp) => {
      this._incrementCounter()
      let status = resp.info().status;
      console.log('response is ', resp.json(), this.state.count, filePath)

    }).catch((err) => {
      this._incrementCounter()
      console.log('error uploading image ', err)
    })


  }

  _incrementCounter = () => {
    let m = this.state.count + 1
    console.log("counter called ", this.state.count, m, m === this.state.images.length)
    if (m === this.state.images.length) {
      this.setState({
        count: m,
        loading: false
      })
      Alert.alert(
        "Success",
        "Images uploaded successfully",
        [
          {
            text: "ok"
          }
        ],
        { cancelable: false }
      )
    } else {
      this.setState({
        count: m
      })
    }
  }

  _onSubmit = () => {
    this._toggleLoader()
    this.state.images.map(item => {
      this._uploadImages(item.path)
    })
  }

  _pickImages = () => {
    ImagePicker.openPicker({
      multiple: true,
      mediaType: "photo"
    }).then(images => {
      let mArray = this.state.images
      if (mArray.length === 0) {
        this.setState({
          images: images
        })
      } else {
        images.map(item => {
          mArray.push(item)
        })
        this.setState({
          images: mArray
        })
      }
    });
  }

  _deleteImage = (index) => {
    let m = this.state.images;
    m.splice(index, 1)
    this.setState({
      images: m
    })
  }

  _renderImages = ({ item, index }) => {
    return <View style={{ flex: .5, margin: 10, height: 144, borderWidth: 1, borderRadius: 6, borderColor: "black", alignContent: "center" }}>
      <Image style={{ width: "100%", height: "100%", alignSelf: "center" }} resizeMode="cover"
        source={{ uri: item.path }} />
      <TouchableOpacity
        style={{ position: "absolute", right: 6, top: 6 }}
        onPress={() => this._deleteImage(index)}>
        <Image style={{ width: 24, height: 24, tintColor: "white" }} resizeMode="center"
          source={require("./delete.png")} />
      </TouchableOpacity>
    </View>
  }

  render() {
    return (
      <View style={{ flex: 1, marginTop: 24, padding: 16, justifyContent: "center" }}>
        {
          this.state.loading ?
            <Spinner
              visible={true}
              textStyle={{ color: "red" }}
            />
            : null
        }

        {
          this.state.images.length ?
            <View style={{ margin: 16 }}>
              <FlatList
                data={this.state.images}
                renderItem={this._renderImages}
                numColumns={2}
                keyExtractor={(index, pos) => pos.toString()} />

            </View>
            : <Text style={{ fontSize: 16, textAlign: "center" }}>No images were selected</Text>
        }
        <View style={{ flexDirection: "row", alignSelf: "center" }}>
          {
            this.state.images.length ?

              <TouchableOpacity
                style={styles.button}
                onPress={() => this._onSubmit()}>
                <Text style={styles.text}>Upload</Text>
              </TouchableOpacity>
              : null
          }
          <TouchableOpacity
            style={styles.button}
            onPress={() => this._pickImages()}>
            <Text style={styles.text}>Pick Images</Text>
          </TouchableOpacity>

        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    margin: 16,
    width: 120,
    alignSelf: "center",
    backgroundColor: "orange",
    borderRadius: 36,
    padding: 16,
    alignContent: "center"
  },
  text: {
    color: "white",
    fontSize: 16,
    textAlign: "center"
  }
})

export default App
